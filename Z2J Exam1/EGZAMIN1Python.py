# Command for player move:
 
def player_turn():
    player_choice = input("Choose one: paper, scisors or stone: ")
    return player_choice
 
import random
# Command for computer move:
 
def computer_turn():
    computer_choice = random.choice(["paper", "scisors", "stone"])
    return computer_choice
 
# Command determining winner:
def win(player_choice, computer_choice):
    if player_choice == computer_choice:
        return("It's a draw!")
    elif (player_choice == "scisors" and computer_choice == "paper") or \
    (player_choice == "stone" and computer_choice == "scisors") or \
    (player_choice == "paper" and computer_choice == "stone"):
        return("You won!")
    else:
        return ("Sorry you lose!")
 
while True:
    player_choice = player_turn()
    computer_choice = computer_turn()
    print(f"You choosed {player_choice}")
    print(f"Computer choosed {computer_choice}")
    print(win(player_choice, computer_choice))
    ask = input("Do you want to play one more time? y/n: ")
    if ask == "n":
        break
 
