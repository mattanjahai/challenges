Rock paper scissors 
Is a hand game, usually played between two people, in which each player simultaneously forms one of three shapes with an outstretched hand.
These shapes are "rock" (a closed fist), "paper" (a flat hand), and "scissors" (a fist with the index finger and middle finger extended, forming a V).
It has three possible outcomes: a draw, a win or a loss. A player who decides to play rock will beat another player who has chosen scissors, 
but will lose to one who has played paper; a play of paper will lose to a play of scissors. If both players choose the same shape,
the game is tied and is usually immediately replayed to break the tie. 