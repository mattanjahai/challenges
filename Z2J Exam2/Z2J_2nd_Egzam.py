from Logo import logo, man_face, game_over
from device import enigma
from door_action import pss_game


class Room:
    def __init__(self, current_room):
        self.current_room = current_room
        
class Door(Room):
    description = f"""You see a blue door, it's the only door in the room,
so it's probably the door to the outside. """
    items = []
    
class Basement(Room):
    description = f"""You are in a dark room, there is no furniture here.
There is a small window on the east wall, opposite the window there are blue door."""
    items = ["hammer", "key", "calculator"]
    

class Window(Room):
    description = f"""You walk over to the window, the only source of light in this small room.
It's an old cellar window in a wooden frame that's tired with time.
It has an alphabetic code LOCK, it is a combination of four letters.
By a strange chance, on the doorframe you can see the inscription 'vwlu' - 7."""
    items = ["lock"]
    
    
hammer = "Hammer, you can definitely break something with it, make it useful."
key = "The key is used to open locks, check if there are any in your sight."
lock = "A window lock with combination of four letters, maybe you have some device that can decode the code"
calculator = "Code-breaking device, just find the code to crack first!"


with open('Room1.txt', 'r', encoding='utf-8') as file1:
    start = file1.read()

with open('Room2.txt', 'r', encoding='utf-8') as file2:
    door_action = file2.read()

 
class Player:

    current_room = Basement
    inventory = []
    game_on = True #when valiurable is going to return False by ending correctly one of quest the game breaks.

    

#function to show items that already are in player inventory           
    def show():
        if len(Player.inventory) == 0:
            print("Your inventory is empty. Try to find something that you can take with you.")
        else:
            print (f"Your inventory: {Player.inventory}")
            
#function to take item from current room to player inventory
    def take_item():
        item = input("What you want to take? < ")
        
        if item not in Player.current_room.items:
            print("There is nothing like that here!")
        else:
            Player.inventory.append(item)
            Player.current_room.items.remove(item)
            
#function showing description of items
    def show_object(obj):
        
        if obj == "hammer":
            if "hammer" in Player.inventory:
                print(hammer)
            else:
                print("You don't have this item")
        elif obj == "key":
            if "key" in Player.inventory:
                print(key)
            else:
                print("You don't have this item")            
        elif obj == "calculator":
            if "calculator" in Player.inventory:
                print(calculator)
            else:
                print("You don't have this item")
        elif obj == "lock":
                if Player.current_room == Window:
                    print(lock)
                else:
                    print("You must be near the lock to see it's description")
        else:
            print("You don't have this item")
            
# function of using already taken items
    def use_item():
        thing = input("Use what? < ").lower()
        
        if thing == "calculator":
            if "calculator" not in Player.inventory:
                print("You dont have anything like that in your inventory!")
            else:
                print("You are using a device that is looking like calculator on the screen you see")
                enigma()
        elif thing == "hammer":
            if "hammer" not in Player.inventory:
                print("You dont have anything like that in your inventory!")
            elif Player.current_room == Door:
                print("You used the hammer on the door, but after a few hits you realize that it is too strong for the hammer to work on it.")
            elif Player.current_room == Window:
                print("You used a hammer, you broke the glass, but there's not enough room to pass, I'm sorry.")
            else:
                print("You have to use the hammer elsewhere.")
        elif thing == "key":
            if "key" not in Player.inventory:
                print("You dont have anything like that in your inventory!")
            elif Player.current_room == Door:
                print(f"""You put the key in the lock and eureka! You unlock the lock .. But wait a minute!
Footsteps are heard!""")
                print(door_action)
                print(man_face)
                pss_game()
                print(game_over)
                Player.game_on = False
            elif Player.current_room == Window:
                print("There is nothing that KEY can interract, it's usefool here.")
            else:
                print("You have to use the Key elsewhere.")
        elif thing == "lock":
            if Player.current_room == Door:
                print("There is no lock at Door")
            elif Player.current_room == Window:
                Player.window_lock()
            else:
                print("There is no lock at Basement")

#function to print description of current room
    def look():
        print(Player.current_room.description)
        if len(Player.current_room.items) > 0:
            print(f"You find some things like:")
            print(Player.current_room.items)
        else:
            print("There is nothing more you can use in here.")
            
#function to enter interaction with window lock
    def window_lock():
        print("You take the lock to better see, you can try to open it.")
        password = input("Type the code lock - ").lower()
        if password == "open":
            print(f"""Congratulations, you have extended the lock. The window handle is easy to open.
Enough space to squeeze easily. Congratulations it's game over! you won your freedom!""")
            print(game_over)
            Player.game_on = False
        else:
            print("Sorry, that's not the code, try again when you will know it.")
            
# function for moving around game rooms             
    def move(Room):
        if Room == "door":
            print(f"You are in front of a big, blue door.")
            Player.current_room = Door
            return Player.current_room
            
        elif Room == "basement":
            print("You are in the middle of the basement.")
            Player.current_room = Basement
            return Player.current_room

        elif Room == "window":
            print("You are near the window.")
            Player.current_room = Window
            return Player.current_room
        
        else:
            print("There is no place like that!")

#function for printing help commands
    def show_help():
        print(f"""Possible commands:
                help             - show this help message
                move [direction] - move to next action game location 
                                  ('window' or 'door' or 'basement')
                take [item]      - take an item from the current room
                show [item]      - description of an item and it's possibile use
                use  [item]      - use item 
                inventory        - show your inventory
                look             - look around the current room""")

# Here starts main loop of the game
print(logo)
start_game = input("\nType 'ready' to begin! - ")
if start_game == "ready":
    print(start)

    while Player.game_on == True:
        action = input("\nWhat is your action? - ").lower()
        
        if action == "inventory":
            Player.show()

        elif action == "move":
            print("Move where?")
            Player.move(input(" < ").lower())

        elif action == "show":
            Player.show_object(input("Enter item: ").lower())                  
                         
        elif action == "take":
            Player.take_item()
            
        elif action == "use":
            Player.use_item()
                
        elif action == "look":
            Player.look()

        elif action == "help":
            Player.show_help()

        elif action == "lock" and Player.current_room == Window:
            Player.window_lock()

        else:
            print("Thanks but there is no action like that, let's try one more time.")
            

