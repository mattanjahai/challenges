nums = [[],[1,2,5,3,1,7,9,1,12,83,1,5,3,2],[1,1,1,1,1,1,1,2,1,1,1],[2,2,2,2],[1,2],[2,1]]
print("Hi, let's sort this")
for i in nums:
    print(i)

def sort(nums):
    for i in range(len(nums)-1, 0, -1):
        for j in range(i):
            if nums[j]>nums[j+1]:
                temp = nums[j]
                nums[j] = nums[j+1]
                nums[j+1] = temp


def do_it():
    for i in nums:
        sort(i)
        print(i)
        print("Sorted!")
    print("Thanks for your time!")
    
while True:
    action = input("Type 'bubble' if you want to start sort\n->")
    if action == "bubble":
        do_it()
        break
    else:
        print("Sorry, wrong spelling, try one more time!")
    
