import random

numbs = [1, 2, 3, 4, 5, 6, 7, 8, 9]
board = []
line = "---------------------------------"

def generate_board():
    for n in range(9):
        random_numbs = random.sample(numbs, 9)
        board.append(random_numbs)
        
      

generate_board()


row1 = [board[i][0:3] for i in range(3)]
row2 = [board[i][3:6] for i in range(3)]
row3 = [board[i][6:9] for i in range(3)]
row4 = [board[i+3][0:3] for i in range(3)]
row5 = [board[i+3][3:6] for i in range(3)]
row6 = [board[i+3][6:9] for i in range(3)]    
row7 = [board[i+6][0:3] for i in range(3)]
row8 = [board[i+6][3:6] for i in range(3)]
row9 = [board[i+6][6:9] for i in range(3)]
    

col1 = [board[0][0], board[0][3], board[0][6], board[3][0], board[3][3], board[3][6], board[6][0], board[6][3], board[6][6]]
col2 = [board[0][1], board[0][4], board[0][7], board[3][1], board[3][4], board[3][7], board[6][1], board[6][4], board[6][7]]
col3 = [board[0][2], board[0][5], board[0][8], board[3][2], board[3][5], board[3][8], board[6][2], board[6][5], board[6][8]]
col4 = [board[1][0], board[1][3], board[1][6], board[4][0], board[4][3], board[4][6], board[7][0], board[7][3], board[7][6]]
col5 = [board[1][1], board[1][4], board[1][7], board[4][1], board[4][4], board[4][7], board[7][1], board[7][4], board[7][7]]
col6 = [board[1][2], board[1][5], board[1][8], board[4][2], board[4][5], board[4][8], board[7][2], board[7][5], board[7][8]]
col7 = [board[2][0], board[2][3], board[2][6], board[5][0], board[5][3], board[5][6], board[8][0], board[8][3], board[8][6]]
col8 = [board[2][1], board[2][4], board[2][7], board[5][1], board[5][4], board[5][7], board[8][1], board[8][4], board[8][7]]
col9 = [board[2][2], board[2][5], board[2][8], board[5][2], board[5][5], board[5][8], board[8][2], board[8][5], board[8][8]]


def check_duplicates_for_list(x):
    check = set()
    for sublist in x:
        for element in sublist:
            if element in check:
                return True  
            check.add(element)
    return False  


def check_duplicates_for_int(x):
    return len(x) != len(set(x))

    
def check_rows():
    if check_duplicates_for_list(row1):
        print("In row 1 there are repetitions that do not follow the rules of sudoku.")
    if check_duplicates_for_list(row2):
        print("In row 2 there are repetitions that do not follow the rules of sudoku.")
    if check_duplicates_for_list(row3):
        print("In row 3 there are repetitions that do not follow the rules of sudoku.")
    if check_duplicates_for_list(row4):
        print("In row 4 there are repetitions that do not follow the rules of sudoku.")
    if check_duplicates_for_list(row5):
        print("In row 5 there are repetitions that do not follow the rules of sudoku.")
    if check_duplicates_for_list(row6):
        print("In row 6 there are repetitions that do not follow the rules of sudoku.")
    if check_duplicates_for_list(row7):
        print("In row 7 there are repetitions that do not follow the rules of sudoku.")
    if check_duplicates_for_list(row8):
        print("In row 8 there are repetitions that do not follow the rules of sudoku.")
    if check_duplicates_for_list(row9):
        print("In row 9 there are repetitions that do not follow the rules of sudoku.")
    else:
        print("There are no repetitions in rows. Correct with sudoku rules.")

def check_columns():
    if check_duplicates_for_int(col1):
        print("In column 1 there are repetitions that do not follow the rules of sudoku.")
    if check_duplicates_for_int(col2):
        print("In column 2 there are repetitions that do not follow the rules of sudoku.")
    if check_duplicates_for_int(col3):
        print("In column 3 there are repetitions that do not follow the rules of sudoku.")
    if check_duplicates_for_int(col4):
        print("In column 4 there are repetitions that do not follow the rules of sudoku.")
    if check_duplicates_for_int(col5):
        print("In column 5 there are repetitions that do not follow the rules of sudoku.")
    if check_duplicates_for_int(col6):
        print("In column 6 there are repetitions that do not follow the rules of sudoku.")
    if check_duplicates_for_int(col7):
        print("In column 7 there are repetitions that do not follow the rules of sudoku.")
    if check_duplicates_for_int(col8):
        print("In column 8 there are repetitions that do not follow the rules of sudoku.")
    if check_duplicates_for_int(col9):
        print("In column 9 there are repetitions that do not follow the rules of sudoku.")
    else:
        print("There are no repetitions in columns. Correct with sudoku rules.")


while True:
    decision = input('\nType 1 for generate board Type 2 for check rows or Type 3 for check columns. If you want to quit type 4\n<')
    if decision == "1":
        print(
        f"{board[0][0:3]} | {board[1][0:3]} | {board[2][0:3]}\n{board[0][3:6]} | {board[1][3:6]} | {board[2][3:6]}\n{board[0][6:9]} | {board[1][6:9]} | {board[2][6:9]}\n{line} \n\
{board[3][0:3]} | {board[4][0:3]} | {board[5][0:3]}\n{board[3][3:6]} | {board[4][3:6]} | {board[5][3:6]}\n{board[3][6:9]} | {board[4][6:9]} | {board[5][6:9]}\n{line}\n\
{board[6][0:3]} | {board[7][0:3]} | {board[8][0:3]}\n{board[6][3:6]} | {board[7][3:6]} | {board[8][3:6]}\n{board[6][6:9]} | {board[7][6:9]} | {board[8][6:9]}\n"
    )  

    elif decision == "2":
        check_rows()
    elif decision == "3":
        check_columns()
    elif decision == "4":
        print("Thanks for your time, bye!")
        break
    else:
        print("There is no such number, try again")
